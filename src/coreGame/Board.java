package coreGame;

import interaction.Choice;

import java.util.ArrayList;

class Board {

    private Cell[][] boardArray = new Cell[9][9];

    // Non utilisé
    // Contructeur par liste
    Board(ArrayList<byte[]> base) {
        for (byte i = 0; i < boardArray.length; i++) {
            for (byte j = 0; j < boardArray[0].length; j++) {
                boolean newCell = false;
                for (byte[] array : base) {
                    if (i == array[0] && j == array[1]) {
                        boardArray[i][j] = new Cell(false, array[2]);
                        newCell = true;
                        break;
                    }
                }
                if (!newCell) {
                    boardArray[i][j] = new Cell(true, (byte)(0));
                }
            }
        }
    }

    // Constructeur par tableau d'entier byte
    Board(byte[][] base) {
    	for (byte i=0; i<9; i++) {
    		for (byte j=0; j<9; j++) {
    		    if (base[i][j] > 0) {
                    boardArray[i][j] = new Cell(false, base[i][j]);
                } else {
                    boardArray[i][j] = new Cell(true, base[i][j]);
                }
            }
    	}
    }
    
    private ArrayList<Byte> numbersInRow(byte i) {
    	ArrayList<Byte> nums = new ArrayList<Byte>();
    	for (byte j=0; j<boardArray[0].length; j++) {
    		if (boardArray[i][j].getValue() != 0) {
    			nums.add(boardArray[i][j].getValue());
    		}
    	}
    	return nums;
    }
    
    private boolean testRow (byte i) {
        byte total = 45;
        for (byte j=0; j<boardArray[0].length; j++) {
            total -= boardArray[i][j].getValue();
        }
        if (total == 0) {
            return true;
        } else {
            return false;
        }
    }
    
    private ArrayList<Byte> numbersInColumn(byte j) {
    	ArrayList<Byte> nums = new ArrayList();
    	for (byte i=0; i<boardArray[0].length; i++) {
    		if (boardArray[i][j].getValue() != 0) {
    			nums.add(boardArray[i][j].getValue());
    		}
    	}
    	return nums;
    }
    
    private boolean testColumn (byte j) {
        byte total = 45;
        for (byte i=0; i<boardArray.length; i++) {
            total -= boardArray[i][j].getValue();
        }
        if (total == 0) {
            return true;
        } else {
            return false;
        }
    }
    
    private ArrayList<Byte> numbersInSquare(byte[] pos) {
    	ArrayList<Byte> nums = new ArrayList<Byte>();
    	pos[0] = (byte)((pos[0]/(byte)(3)) * (byte)(3));
        pos[1] = (byte)((pos[1]/(byte)(3)) * (byte)(3));
        for (byte i=0; i<3; i++) {
            for (byte j=0; j<3; j++) {
                nums.add(boardArray[pos[0]+i][pos[1]+j].getValue());
            }
        }
        
        return nums;
    }
    
    private boolean testSquare (byte[] pos) {
        pos[0] = (byte)((pos[0]/(byte)(3)) * (byte)(3));
        pos[1] = (byte)((pos[1]/(byte)(3)) * (byte)(3));
        byte total = 45;
        for (byte i=0; i<3; i++) {
            for (byte j=0; j<3; j++) {
                total -= boardArray[pos[0]+i][pos[1]+j].getValue();
            }
        }
        if (total == 0) {
            return true;
        } else {
            return false;
        }
    }
    
    boolean testValidChoice (Choice choice) {
    	for (byte num : numbersInRow(choice.getPos()[0])) {
    		if (num == choice.getValue()) {
    			return false;
    		}
    	}
    	
    	for (byte num : numbersInColumn(choice.getPos()[1])) {
    		if (num == choice.getValue()) {
    			return false;
    		}
    	}
    	
    	for (byte num : numbersInSquare(choice.getPos())) {
    		if (num == choice.getValue()) {
    			return false;
    		}
    	}
    	return true;
    }
    
    boolean testValidBoard () {
    	// Lignes
    	for (byte i=0; i<boardArray.length; i++) {
    		if (doubleInList(numbersInRow(i))) {
				return false;
			}
    	}
    	
    	// Colonnes
    	for (byte j=0; j<boardArray.length; j++) {
    		if (doubleInList(numbersInColumn(j))) {
				return false;
			}
    	}
    	
		for (byte i=0; i<boardArray.length; i++) {
    		for (byte j=0; j<boardArray.length; j++) {
    			if (doubleInList(numbersInSquare(new byte[] {i, j}))) {
    				return false;
    			}
    		}
    	}
		return true;
    }
    
    boolean doubleInList (ArrayList<Byte> list) {
    	byte iteration = 0;
		iteration = 0;
		for (byte num : list) {
			if (num != 0) {
				for (byte otherNum : list) {
					if (num == otherNum) {
						iteration++;
					}
				}
			}
		}
		if (iteration > 1) {
			return true;
		} else {
			return false;
		}
    }
    
    boolean testBoard() {
        for (byte i=0; i<boardArray.length; i++) {
            if (!testColumn(i) || !testRow(i)){
                return false;
            }
        }
        for (byte i=0; i<boardArray.length; i+=3) {
            for (byte j=0; j<boardArray[0].length; j+=3) {
                if (!testSquare(new byte[] {i, j})) {
                    return false;
                }
            }
        }
        return true;
    }

    // ##### SET #####

    // Retourne si la valeur a put être modifié
    boolean setValueCell(Choice choice) {     // Return if it has been modified (writable)
        boardArray[choice.getPos()[0]][choice.getPos()[1]].setValue(choice.getValue());
        return boardArray[choice.getPos()[0]][choice.getPos()[1]].getWritable();
    }

    // ##### GET #####

    // Créé un tableau d'entier à partir des cellules du board
    byte[][] getValueArray() {
        byte[][] valueArray = new byte[boardArray.length][boardArray[0].length];
        for (byte i=0; i<boardArray.length; i++) {
            for (byte j=0;j<boardArray[0].length; j++) {
                valueArray[i][j] = boardArray[i][j].getValue();
            }
        }
        return valueArray;
    }
}
