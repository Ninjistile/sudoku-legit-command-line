import coreGame.Game;

public class Main {

	private static Game game;

    public static void main(String[] args) {
        game = new Game();
        while (game.refresh()) {
        }
    }
}
