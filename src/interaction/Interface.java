package interaction;

import java.util.Scanner;

public class Interface {
	
	private static Scanner sc = new Scanner(System.in);

	// Affiche un tableau d' entier donné
	public static void printArray(byte[][] array) {
		
		System.out.print("\\");
		for (byte j=0; j<array[0].length; j++) {
			if (j%3 == 0) {
				System.out.print(' ');
			}
			System.out.print(j+1);
		}
		System.out.println();
		
		for (byte i=0; i<array.length; i++) {
			if (i % 3 == 0) {
				printLine(array[0].length);
			}
			System.out.print(i+1);
			
			for (byte j=0; j<array[0].length; j++) {
				if (j % 3 == 0) {
					System.out.print('|');
				}
				if (array[i][j] == 0) {
                    System.out.print(' ');
                } else {
                    System.out.print(array[i][j]);
                }
			}
			System.out.println('|');
		}
		printLine(array[0].length);
	}
	
	private static void printLine(int lengthJ) {
		System.out.print(' ');
		for (byte j=0; j<lengthJ; j++) {
			if (j % 3 == 0){
				System.out.print('+');
			}
			System.out.print('-');
		}
		System.out.println('+');
	}

	// Rentre les infos de modification d'une cellule
	public static Choice createChoice() {
		String strI, strJ, strValue;		
		// Pour I
		do {
			System.out.println("I : ");
			strI = sc.next();
		} while(!isANumber(strI));
		
		// Pour J
		do {
			System.out.println("J : ");
			strJ = sc.next();
		} while(!isANumber(strJ));
		
		// Pour Value
		do {
			System.out.println("Value : ");
			strValue = sc.next();
		} while(!isANumber(strValue));
		
		byte[] pos = {(byte) (Byte.valueOf(strI)-1), (byte) (Byte.valueOf(strJ)-1)};
		return new Choice(pos, Byte.valueOf(strValue));
	}

	// Test si la chaine est une entier
	private static boolean isANumber (String str) {
		try {
			Integer.valueOf(str);
		} catch(NumberFormatException e) {  // Si ce nest pas un nombre
            return false;
		}
		return true;
	}

	// Demande pour une nouvelle partie
	public static boolean createNewGame() {
		String newGame;
		do {
			System.out.println("Nouvelle partie ? (o : Oui , n : Non)");
			newGame = sc.next();
		} while(!(newGame.equalsIgnoreCase("o")) && !(newGame.equalsIgnoreCase("n")));
		if (newGame.equalsIgnoreCase("o")) {
			return true;
		} else {
			return false;
		}
	}

	// Entrer les valeurs d'une grille nombre par nombre
	public static byte[][] enterTable() {
		byte[][] array = new byte[9][9];
		String strValue;
		System.out.println("Entrez une grille.");
		for (byte i = 0; i < 9; i++) {
			System.out.println("I = " + String.valueOf(i+1));
			for (byte j = 0; j < 9; j++) {
				System.out.println("J = " + String.valueOf(j+1));
				do {
					strValue = sc.next();
				} while (!isANumber(strValue));
				array[i][j] = Byte.valueOf(strValue);
			}
		}
		return array;
	}


	public static void errorWrite(byte i, byte j) {
		System.out.println("La case (" + String.valueOf(i) + ", " + String.valueOf(j) + ") ne peut être réécrite.");
	}

	// Affiche une erreur d'une celule ne pouvant pas être réécrite
	public static void errorWrite(Choice choice) {
        System.out.println("La case (" + String.valueOf(choice.getPos()[0]) + ", " + String.valueOf(choice.getPos()[1])
                + ") ne peut être réécrite.");
    }
	
	public static void noIssueChoiceWrite() {
		System.out.println("Le choix est sans issue.");
	}
}
