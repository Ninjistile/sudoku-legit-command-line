package interaction;

public class Choice {

    private byte[] pos;
    private byte value;

    Choice(byte[] pos, byte value) {
        this.pos = pos;
        this.value = value;
    }
    
    // ##### GET #####
    
    public byte[] getPos() {
    	return pos;
    }
    
    public byte getValue() {
    	return value;
    }
}

