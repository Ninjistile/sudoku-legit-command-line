package coreGame;

import coreGame.Board;
import interaction.Choice;
import interaction.Interface;

public class Game {
    private Board board;
    private Choice choice;
    private boolean continuation;
    private byte[][] arrayBoard;

	public Game() {
	    board = new Board(Interface.enterTable());

	    // Grille de test
	    /* this.arrayBoard = new byte[][]{
                {5, 4, 1, 3, 7, 8, 6, 2, 9},
                {7, 9, 3, 1, 2, 6, 5, 8, 4},
                {2, 8, 6, 9, 5, 4, 7, 1, 3},
                {3, 7, 4, 6, 8, 1, 9, 5, 2},
                {1, 5, 9, 2, 4, 7, 3, 6, 8},
                {6, 2, 8, 5, 3, 9, 4, 7, 1},
                {4, 1, 5, 8, 6, 3, 2, 9, 7},
                {8, 3, 2, 7, 9, 5, 1, 4, 6},
                {9, 6, 7, 4, 1, 2, 8, 3, 5}
		            };
		board = new Board(arrayBoard);*/
	}

	// Renvoie si l'on continue
	public boolean refresh() {
	    Interface.printArray(board.getValueArray());
        choice = Interface.createChoice();
	    if (!board.setValueCell(choice)) {
            Interface.errorWrite(choice);
		}

		if (board.testBoard()) {
            Interface.printArray(board.getValueArray());
            // Si l'on choisie de continuer
            if (Interface.createNewGame()) {
	            board = new Board(Interface.enterTable());
	            return true;
            } else {
	            return false;
            }
        }
        return true;
    }
}