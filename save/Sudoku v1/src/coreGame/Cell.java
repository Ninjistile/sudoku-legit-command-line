package coreGame;

public class Cell {

    private byte value = 0;
    private boolean writable;   // Si la cellule est écris de base, writable est false en ne peut pas être écrite

    Cell (boolean writable, byte value) {
        this.writable = writable;
        this.value = value;
    }


    // ##### GET #####

    byte getValue() {
        return value;
    }

    boolean getWritable() {
        return writable;
    }

    // ###### SET ######

    void setValue(byte newValue) {
        if (writable) {
            value = newValue;
        }
    }
}
